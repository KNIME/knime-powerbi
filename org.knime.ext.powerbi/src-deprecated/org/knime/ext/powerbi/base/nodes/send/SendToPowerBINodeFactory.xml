<?xml version="1.0" encoding="utf-8"?>
<knimeNode icon="./send_to_power_bi.png" type="Sink"
	xmlns="http://knime.org/node/v4.1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://knime.org/node/v4.1 http://knime.org/node/v4.1.xsd"
	deprecated="true">
	<name>Send to Power BI</name>
	<shortDescription>Send a table to Microsoft Power BI.</shortDescription>
	<fullDescription>
		<intro>
			This node sends the input table to Microsoft Power BI.
			<p />
			The node only
			uploads columns that are supported by Power BI. Other
			columns are
			ignored. The supported types are
			<i>String</i>
			,
			<i>Number (integer)</i>
			,
			<i>Number (long)</i>
			,
			<i>Number (double)</i>
			,
			<i>Boolean value</i>
			,
			<i>Local Date</i>
			and
			<i>Local Date Time</i>
			.
			<p />
			The node uploads rows in chunks to Microsoft Power BI. If the node is
			canceled, the already uploaded rows will remain in the Power BI
			dataset.
			<p />
			<b>Important:</b>
			When choosing the <i>Node</i> credentials storage location,
			the node saves a refresh token with access to your Microsoft account
			(with the specified permissions) in the node settings. Before sharing
			the workflow, you should clear your credentials in the node dialog.
			<p />
			The KNIME
			Analytics Platform Azure Application needs the following
			permissions:
			<ul>
				<li>
					<b>View all datasets</b>
					: Needed to check if a dataset already exists in your Power BI
					workspace.
				</li>
				<li>
					<b>Read and write all datasets</b>
					: Needed to upload a table to a Power BI dataset in your workspace.
				</li>
				<li>
					<b>View all workspaces</b>
					: Needed to get the identifier of the selected Power BI workspace.
				</li>
				<li>
					<b>Maintain access to data you have given it access to</b>
					: Needed to access the Power BI API during the node execution
					without asking you to log in again.
				</li>
			</ul>
			<b>Note:</b>
			The Power BI REST API has some limitations in terms of the size of a
			dataset and the number of rows that can be shipped per hour. For more
			information visit the
			<a
				href="https://docs.microsoft.com/en-us/power-bi/developer/api-rest-api-limitations">documentation</a>
			.
		</intro>
		<tab name="Authentication">
			<option name="Authenticate">
				Press this button to authenticate with Power BI.
				A page will be opened in your browser, which will ask you to sign in
				to your Microsoft account. Once you are logged in, you will be asked
				to accept the permission requests of the KNIME Analytics Platform
				application if you did not accept the permissions previously. If the
				authentication succeeded, you will be redirected to a page that
				states that the verification code has been received and that you may
				close the window. The status in the node dialog will update to
				"Authenticated".
			</option>
			<option name="Cancel">
				Cancel a running authentication process.
			</option>
			<option name="Credentials Storage Location">
				Select where authentication credentials should be stored:
				<ul>
				<li>
					<b>Memory</b>
					: The authentication credentials will be kept in memory. 
					They are discarded when closing KNIME. 
				</li>
				<li>
					<b>Local File</b>
					: The authentication credentials will be saved and loaded to and from the selected
					file. 
				</li>
				<li>
					<b>Node</b>
					: The authentication credentials will be saved in the node settings. If you choose this
					option, the workflow will contain your authentication information after is has been saved. 
					Thus, access to your Microsoft account will be available to others if you share the workflow. 
				</li>
			</ul>
			</option>
			<option name="Clear Selected Credentials">
				Deletes the authentication credentials stored at the selected location.
				If <i>Local File</i> was selected, the file will be deleted if
				it contains previously stored credentials. Otherwise, the file will be left
				untouched and an error is displayed. If <i>Node</i> was selected, the credentials will
				be cleared from the node settings <b>not before</b> saving the workflow.
			</option>
			<option name="Clear All Credentials">
				Deletes all stored authentication credentials. Also see description of 
				<i>Clear Selected Credentials</i> button. In case you do not want to share your credentials
				with this workflow, all credentials should be cleared beforehand. 
			</option>
		</tab>
		<tab name="Dataset">
			<option name="Workspace">
				Select the workspace to use. Select "default" to use the default
				workspace which is called "My workspace" in the Power BI user
				interface.
				<br />
				<b>NOTE:</b>
				To see the workspace options you have to be authenticated.
			</option>
			<option name="Create new Dataset">
				Select this option to create a new dataset in the selected Power BI workspace.
			</option>
			<option name="Select existing Dataset">
				Select this option to append the input tables to an existing dataset
				or to overwrite all rows of a table of an existing dataset.
				Note that the dataset has to be a
				<a
					href="https://docs.microsoft.com/en-us/power-bi/service-real-time-streaming#types-of-real-time-datasets">"Push dataset"</a>
				for this option to work.
			</option>
			<option name="Dataset name">
				The name of the dataset to create.
			</option>
			<option name="Table name">
				The name of the table in the dataset to upload. If multiple tables are
				uploaded a name has to be configured for each table and the names have
				to be unique.
			</option>
			<option name="Delete and create new if dataset exists">
				Select this option to delete a dataset with the same name if one exists
				before creating the new dataset. If a dataset with the configured name
				exists but this option is not selected the node will fail.
				<br />
				<b>NOTE: All reports and dashboard tiles containing data from the
					dataset will also be deleted.
				</b>
			</option>
			<option name="Append/Overwrite rows">
				Select if the input tables should be appended to the existing tables or
				if all rows of the exiting table should be deleted and overwritten by
				the input rows.
			</option>
		</tab>
	</fullDescription>
	<ports>
		<inPort index="0" name="Table">
			Data to be send to Power BI.
		</inPort>
		<dynInPort insert-before="1" group-identifier="input" name="Additional input table">
			Additional data to be send to your data set.
		</dynInPort>
 </ports>
</knimeNode>