# ![Image](https://www.knime.com/files/knime_logo_github_40x40_4layers.png) KNIME® Power BI Integration

This repository is maintained by the [KNIME Team Quokkas](mailto:scrum-bd-esi@knime.com).

This repository contains the plugins for the KNIME Power BI Integration.
The Power BI integration contains a node which allows sending a dataset
to the Power BI service.

## Overview

* _org.knime.ext.azuread_: Execute OAuth authentication on Azure Active Directory.
* _org.knime.ext.powerbi_: Send datasets to Power BI via the Power BI REST API.

## Development Notes

You can find instructions on how to work with our code or develop extensions for
KNIME Analytics Platform in the _knime-sdk-setup_ repository
on [BitBucket](https://bitbucket.org/KNIME/knime-sdk-setup)
or [GitHub](http://github.com/knime/knime-sdk-setup).

## Join the Community!

* [KNIME Forum](https://tech.knime.org/forum)

